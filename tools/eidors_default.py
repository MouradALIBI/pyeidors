#
from solvers.forward.fwd_solve_1st_orde import fwd_solve_1st_orde


def mk_varargout_str(N):
    output = '['
    for i in range(N):
        output += 'varargout{'+str(i)+'} '
    output += ']'
    return output


def get_default(caller):
    default = "fwd_solve_1st_orde"
    return default


def call_default(caller, varargin):
    default = get_default(caller)
    #output = mk_varargout_str(1)
    varargout = []
    data = fwd_solve_1st_orde()
    varargout = 0
    return  varargout



def eidors_default(varargin):
    caller = "fwd_solve"
    # output = mk_varargout_str(1)
    varargout = []
    varargout = call_default(caller,varargin)
    varargout = 0
    return varargout