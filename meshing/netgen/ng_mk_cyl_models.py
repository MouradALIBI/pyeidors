# NG_MAKE_CYL_MODELS: create cylindrical models using netgen
# [fmdl,mat_idx] = ng_mk_cyl_models(cyl_shape, elec_pos, ...
#                 elec_shape, extra_ng_code);
# INPUT:
# cyl_shape = {height, [radius, [maxsz]]}
#    if height = 0 -> calculate a 2D shape
#    radius (OPT)  -> (default = 1)
#    maxsz  (OPT)  -> max size of mesh elems (default = course mesh)
#
# ELECTRODE POSITIONS:
#  elec_pos = [n_elecs_per_plane,z_planes]
#     OR
#  elec_pos = [degrees,z] centres of each electrode (N_elecs x 2)
#
# ELECTRODE SHAPES::
#  elec_shape = [width,height, maxsz]  # Rectangular elecs
#     OR
#  elec_shape = [radius, 0, maxsz ]    # Circular elecs
#     OR
#  elec_shape = [0, 0, maxsz ]         # Point elecs
#    (point elecs does some tricks with netgen, so the elecs aren't exactly where you ask)
#
# Specify either a common electrode shape or for each electrode
#
# EXTRA_NG_CODE
#   string of extra code to put into netgen geo file. Normally this
#   would be to insert extra materials into the space
#
# OUTPUT:
#  fmdl    - fwd_model object
#  mat_idx - indices of materials (if extra_ng_code is used)
#    Note mat_idx does not work in 2D. Netgen does not provide it.
#
from eidors_obj import eidors_obj

import scipy.io

from models.mk_image import mk_image


def ng_mk_cyl_models(f_mdl, cyl_shape, elec_pos, elec_shape, extra_ng_code=['', '']):
    cache_obj = [cyl_shape, elec_pos, elec_shape, extra_ng_code]
    # fmdl = eidors_obj('get-cache', cache_obj, 'ng_mk_cyl_models')
    #
    # create cylindrical models using netgen
    # CCW starting from bottom (as Kadir is measuring)
    # f_mdl.electrode = f_mdl.electrode([5:-1:1,8:-1:6]);
    fmdl = scipy.io.loadmat('f_mdl.mat')
    fmdl = ((fmdl['f_mdl'])[0])[0]
    f_mdl.type = fmdl['type']
    f_mdl.name = fmdl['name']
    f_mdl.nodes = fmdl['nodes']
    f_mdl.elems = fmdl['elems']
    f_mdl.boundary = fmdl['boundary']
    f_mdl.gnd_node = fmdl['gnd_node']
    f_mdl.mat_idx = fmdl['mat_idx']
    f_mdl.mat_idx_reordered = fmdl['mat_idx_reordered']
    f_mdl.electrode = (fmdl['electrode'])[0]
    f_mdl.solve = fmdl['solve']
    f_mdl.jacobian = fmdl['jacobian']
    f_mdl.system_mat = fmdl['system_mat']
    f_mdl.normalize_measurements = fmdl['normalize_measurements']
    f_mdl.stimulation = fmdl['stimulation'][0]
    # ---------- Homogeneous background ------------
    img_backg = mk_image(f_mdl, 1)

    return f_mdl, img_backg
