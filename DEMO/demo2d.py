

# coding: utf-8
#
# Copyright (c) 2018 The PyEIDORS Authors. All Rights Reserved.
#
#      alibi@hrz.tu-chemnitz.de
#      mourad.alibi@enis.tn
#
# limitations under the License.
# Distributed under the (new) BSD License. See LICENSE.txt for more info.
# ==============================================================================

"""""
import scipy.io
global eidors_colours = scipy.io.loadmat('../graphics/eidors_colours.mat')['eidors_colours']
"""""
import timeit


start = timeit.default_timer()

import sys

sys.path.append('..')
from builtins import KeyboardInterrupt

from matplotlib import figure
from graphics.show_fem import show_fem
from solvers.inv_solve import inv_solve

# from overloads.sparse import sparse
from solvers.fwd_solve import fwd_solve

# import message as msg

from meshing.netgen.ng_mk_cyl_models import ng_mk_cyl_models
from meshing.doraMesh.demo_distmesh2d import  circle_mesh


class f_mdl:
    def __init__(self, type, name, nodes, elems, boundary, gnd_node, mat_idx, mat_idx_reordered, electrode, solve,
                 jacobian, system_mat, normalize_measurements, stimulation, n_elem):
        self.type = type
        self.name = name
        self.nodes = nodes
        self.elems = elems
        self.boundary = boundary
        self.gnd_node = gnd_node
        self.mat_idx = mat_idx
        self.mat_idx_reordered = mat_idx_reordered
        self.electrode = electrode
        self.solve = solve
        self.jacobian = jacobian
        self.system_mat = system_mat
        self.normalize_measurements = normalize_measurements
        self.stimulation = stimulation
        self.n_elem = n_elem


class data:
    def __init__(self, meas, time, name, type):
        self.meas = meas
        self.time = time
        self.name = name
        self.type = type


class i_mdl:
    class hyperparameter:
        def __init__(self, value):
            self.value = value

    class jacobian_bkgnd:
        def __init__(self, value):
            self.value = value

    def __init__(self, type, name, fwd_model, solve, hyperparameter, jacobian_bkgnd, reconst_type, RtR_prior):
        self.type = type
        self.name = name
        self.fwd_model = fwd_model
        self.solve = solve
        self.hyperparameter = hyperparameter
        self.jacobian_bkgnd = jacobian_bkgnd
        self.reconst_type = reconst_type
        self.RtR_prior = RtR_prior


class opts:
    def __init__(self, abs_solve, select_parameters, reconst_to_elems, scale):
        self.abs_solve = abs_solve
        self.select_parameters = select_parameters
        self.reconst_to_elems = reconst_to_elems
        self.scale = scale


def destroy():
    print("ERROR!")


def create_inv_model(f_mdl, vh, vi, i_mdl):
    #  Get a 2D image reconstruction model
    i_mdl.type = 'inv_model'
    i_mdl.name = 'inverse model'
    i_mdl.fwd_model = f_mdl
    # inverse model and reconstruct
    # One step Gauss-Newton reconstruction (NOSER prior)
    # i_mdl.solve =@inv_solve_diff_GN_one_step
    i_mdl.hyperparameter.value = 0.00003
    i_mdl.hyperparameter.value = 0.1
    i_mdl.jacobian_bkgnd.value = 1000
    i_mdl.reconst_type = 'difference'
    # i_mdl.RtR_prior =@prior_noser
    return i_mdl


def show_difference_fem(img, fig_title):
    figure
    femi = show_fem(img, [True, True])
    pass


if __name__ == "__main__":
    try:
        # ---------- Start clock ----------
        # tic
        # ---------- Variables ------------

        n_elecs = 16  # number of electrodes
        n_rings = 1  # number of rings
        I = 0.01  # 0.9e-6 # very weak AC injection current -- [DEFAULT = 0.010 Amp]
        # ---------- Fwd model ------------
        height = 0
        radius = 1
        maxsz = 0.02  # result in ==> 14306 points, 61515 elements # 0.025 would
        # result in ==> 17925 points, 73735 elements
        e_radius = 0.3
        injection = 'ad'
        acquisition = 'ad'
        load_pos = 'right'
        # create cylindrical models using netgen
        f_mdl, img_backg = ng_mk_cyl_models(f_mdl, [height, radius, maxsz], [n_elecs, n_rings], [e_radius, 0])
        #######################
        # -- Fwd Simulation-- #
        #######################
        v_backg = fwd_solve(f_mdl, img_backg, data)
        v_strain, v_strain_with_struc = v_backg, v_backg
        # ---------- No load Measurements --------------
        v_backg.name = 'Homogeneous voltages'
        # ---------- Right Load ------------------------
        v_strain.name = 'Inhomogeneous voltages'
        # ---------- with load Right -------------------
        v_strain_with_struc.meas = [-1.4325, -0.1911, 0.0234, 0.0105, -0.1351, -0.0719, -0.1253, -0.1435, -0.1745,
                                    -0.1660, -0.1486, -0.0463, -0.0357, -1.8950, -0.4914, -0.2811, -0.0469, -0.0868,
                                    -0.0276, -0.0831, -0.0137, -0.0105, -0.0185, -0.0654, -0.1233, -0.1884, -0.4293,
                                    -1.7846, -0.3599, -0.0433, -0.0140, 0.0704, 0.0431, 0.0595, 0.0113, 0.0456, 0.0078,
                                    0.0529, 0.0052, -0.2586, 0.0176, -1.8891, -0.3628, -0.1185, -0.0129, 0.0054,
                                    -0.0287, -0.0381, 0.0210, -0.0206, -0.0278, 0.0050, -0.0955, 0.0096, -0.3000,
                                    -1.7922, -0.3267, -0.1737, -0.0987, -0.0345, -0.0624, 0.0156, -0.0387, -0.0233,
                                    -0.0462, 0.0057, 0.0136, -0.1000, -0.2489, -1.8913, -0.5382, -0.2869, -0.0412,
                                    -0.0037, -0.0715, 0.0244, 0.0357, -0.0108, 0.0008, 0.0119, -0.0287, -0.0385,
                                    -0.1724, -1.8807, -0.4726, -0.1204, -0.0750, -0.0285, -0.0666, 0.0323, 0.0087,
                                    -0.0036, 0.0108, -0.0895, -0.0781, -0.1132, -0.2896, -1.9657, -0.4091, -0.1580,
                                    -0.1280, -0.0379, -0.0471, 0.0111, -0.0203, -0.0144, -0.0028, -0.0139, -0.0015,
                                    -0.0235, -0.3491, -1.8110, -0.3903, -0.0859, -0.0049, 0.0035, 0.0094, 0.0053,
                                    -0.0050, -0.0165, -0.0133, -0.0759, 0.0184, -0.1021, -0.1686, -1.8743, -0.4464,
                                    -0.2169, -0.1062, 0.0070, -0.0283, 0.0648, 0.0171, -0.0372, -0.0674, -0.0712,
                                    -0.0647, -0.1248, -0.2920, -1.9222, -0.4496, -0.2011, -0.1367, -0.0464, 0.0766,
                                    0.0052, 0.0234, -0.0259, 0.0115, -0.0200, -0.0812, -0.1088, -0.2471, -1.8216,
                                    -0.3530, -0.0974, -0.0222, 0.0774, -0.0058, -0.0313, -0.0598, -0.0178, -0.0298,
                                    -0.0192, -0.0450, -0.0205, -0.3226, -1.6859, -0.3275, -0.1962, 0.0695, -0.0876,
                                    -0.0073, -0.0146, 0.0031, -0.0417, -0.0036, -0.0076, -0.0823, -0.1140, -0.2961,
                                    -1.5843, -2.0113, -0.4229, -0.8871, -0.5868, -0.5512, -0.5379, -0.5730, -0.5537,
                                    -0.4802, -0.4796, -0.5983, -0.6711, -0.6230, -0.4937, -0.8470, -0.6366, -0.5606,
                                    -0.5258, -0.5843, -0.5357, -0.4804, -0.5289, -0.5970, -0.6146, -0.6280, -0.8573]
        i_mdl = create_inv_model(f_mdl, v_backg.meas, v_strain.meas, i_mdl)
        img_strain_diff = inv_solve(i_mdl, v_strain, v_backg, opts)


        #### img_strain = inv_solve(i_mdl, v_strain)
        show_difference_fem(img_strain_diff, (
                    'Exp -- Injection ' + injection + ' | Acquisition ' + acquisition + ' -- Load ' + load_pos))
#TODO: is a function in /home/alibi/Desktop/PyEIDORS_ENIS/solvers/calc_jacobian.py", line 62

        circle_mesh()

        k = 5252
    except KeyboardInterrupt:
        destroy()

stop = timeit.default_timer()

print(stop - start)
