
# !/usr/bin/env python3
# coding: utf-8
#
# Copyright (c) 2018 The PyEIDORS Authors. All Rights Reserved.
#
#      alibi@hrz.tu-chemnitz.de
#      mourad.alibi@enis.tn
#
# limitations under the License.
# Distributed under the (new) BSD License. See LICENSE.txt for more info.
# ==============================================================================


"""""
# ![PyEIDORS]()

Thank you for the interest in `PyEIDORS`!

`PyEIDORS` is **a python-based, framework of Electrical Impedance Tomography (EIT).**

"""""
import time

import sparse
import sys
from builtins import KeyboardInterrupt

from numpy import unique
import numpy as np
import matplotlib.pyplot as plt

# from overloads.sparse import sparse
from sparse import COO


sys.path.append('..')
from solvers.forward.find_boundary import find_boundary
# import message as msg
import scipy.io
import scipy.sparse
from deprecated.set_3d_currents import set_3d_currents
from deprecated.get_3d_meas import get_3d_meas
from solvers.fwd_solve import fwd_solve


class stimulat:
    def __init__(self, stimulation, stim_pattern, meas_pattern):
        self.stimulation = stimulation
        self.stim_pattern = stim_pattern
        self.meas_pattern = meas_pattern


class electrod:
    def __init__(self, z_contact, nodes):
        self.z_contact = z_contact
        self.nodes = nodes

        #########################
        ##    destroy          ##
        #########################


class homg_img:
    def __init__(self, name, type, elem_data, fwd_model):
        self.name = name
        self.type = type
        self.elem_data = elem_data
        self.fwd_model = fwd_model



def destroy():
    print("ERROR!")

    #########################
    ##  1- Creat FEM Model ##
    #########################


def create_fem_model():
    #
    # create a 'fwd_model' object with name demo_mdl
    #
    # bdy : the boundary surfaces (triangles)
    # vtx : the vertices of the model (coordinates of the nodes)
    # simp: the simplices of the model (connectivity in tetrahedral)
    #

    mat = scipy.io.loadmat('../sample_data/datareal.mat')
    vtx = mat['vtx']
    simp = mat['simp']
    bdy = mat['bdy']
    # bdy = find_boundary(simp)
    return vtx, simp, bdy

    #########################
    ##       MAIN          ##
    #########################


def get_model_elecs():
    # elec : The electrodes matrix.
    # np_pl : Number of electrode planes (in planar arrangements)
    # protocol : Adjacent or Opposite or Customized.
    # zc : Contact impedances of the electrodes
    # perm_sym : Boolean entry for efficient forward computations
    # perm_sym='{n}';
    mat2 = scipy.io.loadmat('../sample_data/datareal.mat')
    gnd_ind = mat2['gnd_ind']
    elec = mat2['elec']
    zc = mat2['zc']
    protocol = mat2['protocol']
    no_pl = mat2['no_pl']

    # print(gnd_ind,elec,zc,protocol,no_pl)
    electrodes = []
    i = 0
    for z in zc:
        e = electrod(z, unique(elec[i]))
        # electrod.z_contact = z
        # electrod.nodes = unique(elec[i])
        i = i + 1
        electrodes.append(e)

    # print(electrodes)
    perm_sym = '{n}';
    return gnd_ind, electrodes, perm_sym, elec, protocol, no_pl


def get_model_stim(mdl):
    # get the current stimulation patterns

    mat3 = scipy.io.loadmat('../sample_data/datareal.mat')
    protocol = mat3['protocol']
    no_pl = int(mat3['no_pl'])
    elec = mat3['elec']
    I, Ib = set_3d_currents(protocol, elec, mdl.nodes, mdl.gnd_node, no_pl)
    # get the measurement patterns, only indH is used in this model
    #   here we only want to get the meas pattern from 'get_3d_meas',
    #   not the voltages, so we enter zeros
    jnk, jnk, indH, indV, jnk = get_3d_meas(elec, mdl.nodes, np.zeros(shape=(len(I), len(I[0]))), Ib,
                                            no_pl)  # zeros(size(I)) : Vfwd
    n_elec = len(elec)
    n_meas = int(len(indH) / len(Ib[0]))
    stimulations = []
    for i in range(len(Ib[0])):
        idx = list(range(i * n_meas, (i + 1) * n_meas))
        #### create a1 = [0,0 ; 1,1 ; 2,2; ... ; n_meas,n_meas ] Matrix :: {n_meas x 2}  ==>> (1:n_meas)'*[1,1]
        la = []
        la = list(range(n_meas))
        ka = []
        ka.append(la)
        ka.append(la)
        a1 = ka
        #####################################################
        ka2 = [indH[i] for i in idx]
        a2 = np.transpose(ka2)
        ### creat    = [1 , -1 ;1 , -1 ;1 , -1 ;1 , -1 ; ... ]  Matrix :: {  {n_meas x 2}  ==>>  ones(n_meas,2)*[1,0;0,-1]
        lo = np.ones(n_meas)
        li = lo * -1
        a3 = []
        a3.append(lo)
        a3.append(li)

        #####################################################

        # S  = sparse.coo_matrix((a3, (a1, a2)), shape=(,n_elec).)
        # meas_pat = scipy.sparse(a1,a2,a3,n_meas,n_elec)
        # meas_pat = scipy.sparse((1:n_meas)*[1,1], indH(idx,: ), ones(n_meas, 2) * [1, 0;0, -1], n_meas, n_elec );

        s1 = sparse.COO((a1[0], a2[0]), a3[0], shape=(n_meas, n_elec))
        s2 = sparse.COO((a1[1], a2[1]), a3[1], shape=(n_meas, n_elec))

        s = s1 + s2

        meas_pat = s.todense()

        ################################################
        ################# Display matrix################
        ################################################

        def samplemat(dims):
            """Make a matrix with all zeros and increasing elements on the diagonal"""
            aa = np.zeros(dims)
            for i in range(min(dims)):
                aa[i, i] = i
            return aa

        # Display matrix
        # plt.matshow(meas_pat)
        # plt.show()

        sti = stimulat('Amp', Ib[:, i], meas_pat)
        stimulations.append(sti)

    return stimulations


if __name__ == "__main__":
    import cProfile


   ## cProfile.runctx('foo()', None, locals())
    try:
        tmps1 = time.clock()
        print('step 1: ceate FEM model structure')
        vtx, simp, bdy = create_fem_model()


        # print("vtx:", vtx, "simp", simp, "bdy", bdy)

        class demo_mdl:
            class np_fwd_solve:
                def __init__(self, perm_sym):
                    self.perm_sym = perm_sym

            def __init__(self, name, nodes, elems, boundary, solve, jacobian, system_mat, gnd_node, electrode,
                         np_fwd_solve, stimulation, type, normalize_measurements, n_elem, n_node, n_elec):
                self.name = name
                self.nodes = nodes
                self.elems = elems
                self.boundary = boundary
                self.solve = solve
                self.jacobian = jacobian
                self.system_mat = system_mat
                self.gnd_node = gnd_node
                self.electrode = electrode
                self.np_fwd_solve = np_fwd_solve
                self.stimulation = stimulation
                self.type = type
                self.normalize_measurements = normalize_measurements
                self.n_elem = n_elem
                self.n_node = n_node
                self.n_elec = n_elec


        demo_mdl.name = 'demo real model'
        demo_mdl.nodes = vtx
        demo_mdl.elems = simp
        demo_mdl.boundary = bdy
        demo_mdl.solve = 'np_fwd_solve'
        demo_mdl.jacobian = 'np_calc_jacobian'
        demo_mdl.system_mat = 'np_calc_system_mat'

        print('step 2: create FEM model electrodes definitions')
        [gnd_ind, electrodes, perm_sym, elec, protocol, no_pl] = get_model_elecs()

        demo_mdl.gnd_node = gnd_ind
        demo_mdl.electrode = electrodes
        demo_mdl.np_fwd_solve.perm_sym = perm_sym

        print('step 3: create FEM model stimulation and measurement patterns')
        stimulations = get_model_stim(demo_mdl)
        demo_mdl.stimulation = stimulations
        demo_mdl.type = "fwd_model"

        print("step 4: simulate data for homogeneous medium")
        #####################################################
        #######      create a homogeneous image       #######
        #####################################################
        # mat = ones(size(demo_mdl.elems, 1), 1);
        mat = np.ones(len(demo_mdl.elems))
        homg_img = homg_img('homogeneous image', 'image', mat, demo_mdl)
# TODO: Complite fonction "fwd_solve(demo_mdl, homg_img)"
# TODO: Remarque   TypeError: fwd_solve() missing 1 required positional argument: 'data'

        #homg_data = fwd_solve(demo_mdl, homg_img)

        # setup()
        # loop()
        aaa = 5687687
        tmps2 = time.clock()
        print(tmps2 - tmps1)
    except KeyboardInterrupt:
        destroy()
