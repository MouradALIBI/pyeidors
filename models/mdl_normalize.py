class mdl_normalize:
    """
    MDL_NORMALIZE Check or set the normalize_measurements flag on a model.
      OUT = MDL_NORMALIZE(MDL) returns the value of the
      'normalize_measurements' or 'normalize' field on MDL. If absent, the
      default value is returned as set with EIDORS_DEFAULT.

      MDL = MDL_NORMALIZE(MDL, VAL) sets the 'normalize_measurements' to VAL

    """

    def __init__(self, mdl, val=0):
        self.mdl = mdl
        self.val = val

    def get_flag(mdl):
        out = []
        # iterate over fields
        print(dir(mdl))
        ff =[a for a in dir(mdl) if not a.startswith('__')]
        for i in ff :
            if i == 'normalize' or i =='normalize_measurements':
                out = 0
        print()
        out = 0
        return out

    def set_flag(mdl, val):
        out = 0
        return out
