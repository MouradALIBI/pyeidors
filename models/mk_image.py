import numpy as np


def fill_in_data(img, elem_data, params):
    # If image is presented as row vector, then transpose
    sz = len(img.fwd_model.elems)
    img.elem_data = np.ones(sz, int)
    return img


def mk_image(mdl, elem_data, params=0, name=0):
    """
    MK_IMAGE: create eidors image object
    img= mk_image(mdl, elem_data, name)
    :param mdl:
    :param elem_data:
    :param params:
    :param name:
    :return:
    """

    class img:
        def __init__(self, type, name, fwd_model, elem_data,n_elem,current_params):
            self.type = type
            self.name = name
            self.fwd_model = fwd_model
            self.elem_data = elem_data
            self.n_elem = n_elem
            self.current_params = current_params

    default_params = 'unspecified'  # later: conductivity ?
    default_name = 'Created by mk_image'
    name = default_name
    params = default_params
    if mdl.type == 'fwd_model':
        mdl = mdl  # keep model
    img.name = 'image'
    img.type = 'Created by mk_image'
    img.fwd_model = mdl
    img = fill_in_data(img, elem_data, params)  # img.current_params = params;
    #img.current_params = params
    return img
