'''''
b = nchoosek(n,k) returns the binomial coefficient, defined as n!/((n–k)! k!). 
This is the number of combinations of n items taken k at a time.
C = nchoosek(v,k) returns a matrix containing all possible combinations 
of the elements of vector v taken k at a time. Matrix C has k columns and
 n!/((n–k)! k!) rows, where n is length(v).
'''''
import itertools


def nchoosek(v, k):
    c = []
    c = itertools.combinations(v,k)
    # n = len(v) + 1
    # if n == k:
    #     c = v
    # elif n == k + 1:
    #     c = repmat (v,n,1)
    return c
