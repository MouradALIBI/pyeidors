import math
import numpy as np
import scipy
from scipy import sparse
from scipy.sparse import isspmatrix

from models.mdl_normalize import mdl_normalize


class copt:
    def __init__(self, fstr, log_level):
        self.fstr = fstr
        self.log_level = log_level


# calculate element volume and surface area
def element_volume(NODE, ELEM, e, d):
    VOLUME = np.zeros(e)
    ones_d = np.transpose(np.ones(d))
    d1fac = math.factorial(d - 1)
    ELEM -= 1
    if d > len(NODE):
        for i in range(e):
            this_elem = NODE[:, ELEM[:, i]]
            VOLUME[i] = (np.abs(np.linalg.det(np.transpose(np.vstack((ones_d, this_elem)))))) / d1fac
    # elif d == 3 :   #3D nodes in 2D mesh
    #     for i=1:e
    #     this_elem = NODE(:, ELEM(:, i));
    #     d12 = det([ones_d;
    #     this_elem([1, 2],:)]) ^ 2;
    #     d13 = det([ones_d;
    #     this_elem([1, 3],:)]) ^ 2;
    #     d23 = det([ones_d;
    #     this_elem([2, 3],:)]) ^ 2;
    #     VOLUME(i) = sqrt(d12 + d13 + d23) / d1fac;
    # elif d == 2 :#3D nodes in 1D mesh (ie resistor mesh)
    #     for i=1:e
    #     this_elem = NODE(:, ELEM(:, i));
    #     d12 = det([ones_d;
    #     this_elem([1],:)]) ^ 2;
    #     d13 = det([ones_d;
    #     this_elem([2],:)]) ^ 2;
    #     d23 = det([ones_d;
    #     this_elem([3],:)]) ^ 2;
    #     VOLUME(i) = sqrt(d12 + d13 + d23) / d1fac;
    # else:
    #     print('warning:mesh size not understood when calculating volumes')
    #     VOLUME = NaN
    return VOLUME


def calculate_N2E(fwd_model, bdy, n_elec, n):
    cem_electrodes = 0  # num electrodes part of Compl. Elec Model
    N2E = np.zeros(shape=(n_elec, n + n_elec))
    for i in range(n_elec):
        elec_nodes = fwd_model.electrode[i][0][0]
        cem_electrodes = cem_electrodes + 1
        N2E[i, n + cem_electrodes - 1] = 1
    N2E = sparse.csr_matrix(N2E)
    return N2E


def calc_QQ_fast(N2E, stim, p):
    QQ = np.zeros(N2E.shape[1])
    QQ[N2E.shape[1] - 1] = p
    stimul = scipy.io.loadmat('model_stim.mat')['ans']
    QQ = sparse.csr_matrix(np.transpose(N2E.todense())*stimul.todense())
    n_meas = 208 # modele_stime :: 16*13 = 208
    return QQ, n_meas


def calc_param(fwd_model):
    # perform actual parameter calculation
    class pp:
        def __init__(self, n_elem, n_elec, n_node, n_stim, n_dims, n_meas, bounda, NODE, ELEM, QQ, VOLUME, normal, N2E):
            self.n_elem = n_elem
            self.n_elec = n_elec
            self.n_node = n_node
            self.n_stim = n_stim
            self.n_dims = n_dims
            self.n_meas = n_meas
            self.bounda = bounda
            self.NODE = NODE
            self.ELEM = ELEM
            self.QQ = QQ
            self.VOLUME = VOLUME
            self.normal = normal
            self.N2E = N2E
    pp.NODE = np.transpose(fwd_model.nodes)
    pp.ELEM = np.transpose(fwd_model.elems)
    n = len(pp.NODE[0])  # NODEs
    d = len(pp.ELEM)  # dimentions + 1
    e = len(pp.ELEM[0])  # ELEMents
    p = len(fwd_model.stimulation)
    n_elec = len(fwd_model.electrode)
    copt.fstr = 'element_volume'
    copt.log_level = 4
    pp.VOLUME = element_volume(pp.NODE, pp.ELEM, e, d)
    bdy = fwd_model.boundary
    """
    Matrix to convert Nodes to Electrodes
    Complete electrode model for all electrodes
    N2E = sparse(1:n_elec, n+ (1:n_elec), 1, n_elec, n+n_elec);
    pp.QQ= sparse(n+n_elec,p);
    """
    N2E = calculate_N2E(fwd_model, bdy, n_elec, n)
    if p > 0:
        stim = fwd_model.stimulation[0]
        [pp.QQ, pp.n_meas] = calc_QQ_fast(N2E, stim, p)
    # pack into a parameter return list
    pp.n_elem = e
    pp.n_elec = n_elec
    pp.n_node = n
    pp.n_stim = p
    pp.n_dims = d - 1
    pp.N2E = N2E
    pp.boundary = bdy
    pp.normalize = mdl_normalize(fwd_model)
    return pp


def fwd_model_parameters(fwd_model):
    """
    :param fwd_model:
    :return:
     FWD_MODEL_PARAMETERS: data= fwd_solve_1st_order( fwd_model, image)
 Extract parameters from a 'fwd_model' struct which are 
 appropriate for Andy Adler's EIT code
   param.n_elem     => number of elements
   param.n_elec     => number of electrodes
   param.n_node     => number of nodes (vertices)
   param.n_stim     => number of current stimulation patterns
   param.n_elec     => number of electrodes
   param.n_dims     => dimentions (2= 2D, 3=3D)
   param.n_meas     => number of measurements (total)
   param.boundary   => FEM boundary
   param.NODE       => vertex matrix
   param.ELEM       => connection matrix
   param.QQ         => Current into each NODE
   param.VOLUME     => Volume (or area) of each element
   param.normalize  => difference measurements normalized?
   param.N2E        => Node to electrode converter

 If the stimulation patterns has a 'interior_sources' field,
   the node current QQ, is set to this value for this stimulation.

    """
    copt.fstr = 'fwd_model_parameters'
    copt.log_level = 4
    param = calc_param(fwd_model)
    return param
