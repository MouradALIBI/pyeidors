from solvers.Copt import Copt

# Make the Jacobian only depend on
def jacobian_cache_params(fwd_model, img):
    img.current_params = 'conductivity'
    try:
        cache_obj = [fwd_model, img.elem_data, img.current_params]
        return cache_obj
    except:
        try:
            cache_obj = {fwd_model, img.node_data, img.current_params}
            return cache_obj
        except:
            print("error('calc_jacobian: execting elem_data or node_data in image')")
    pass


def calc_jacobian( fwd_model, img=None):
    """
 CALC_JACOBIAN: calculate jacobian from an inv_model
 
  J = calc_jacobian( img )
      calc Jacobian on img.fwd_model at conductivity given
      in image (fwd_model is for forward and reconstruction)
 
 The actual work is done by the jacobian calculator specified in 
    img.fwd_model.jacobian, unless that field is numeric, in which case
    calc_jacobian returns its contents.

 For reconstructions on dual meshes, the interpolation matrix
    is defined as img.fwd_model.coarse2fine. This takes
    coarse2fine * x_coarse = x_fine

 If the underlying jacobian calculator doesn't understand dual
    meshes, then calc_jacobian will automatically postmultiply
    by fwd_model.coarse2fine.

 img       is an image structure, with 'elem_data' or
           'node_data' parameters

    """
    if fwd_model:
        img = fwd_model
    else:
        print("warning('EIDORS:DeprecatedInterface [ Calling CALC_JACOBIAN with two arguments is deprecated and will "
              "cause an error in a future version. First argument ignored.])")

    print("warning('query','EIDORS:DeprecatedInterface');")
    try:
        fwd_model = img.fwd_model;
    except:
        print("error('CALC_JACOBIAN requires an eidors image structure')")

    if( isinstance(fwd_model.mdl.jacobian, int )): # we have the Jacobian matrix
        J = fwd_model.jacobian
    else: # we need to calculate
        copt = Copt()
        copt.cache_obj = jacobian_cache_params(fwd_model, img);
        copt.fstr = 'jacobian'
        #try:
        #     fwd_model.jacobian = str2func(fwd_model.jacobian)
        # end
        # J = eidors_cache(fwd_model.jacobian, {fwd_model, img}, copt);
        # j = eid
    J = 0
    return J