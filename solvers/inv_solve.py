from models.mdl_normalize import mdl_normalize
from solvers.inverse.calc_solution_error import calc_solution_error
from solvers.inverse.inv_solve_diff_GN_one_step import inv_solve_diff_GN_one_step


def prepare_model(mdl):
    fmdl = mdl.fwd_model
    fmdl = mdl_normalize(fmdl, mdl_normalize(fmdl))

    mdl.fwd_model = fmdl
    #  warn if we have deprecated inv_model.parameters laying about
    # mdl = deprecate_imdl_parameters(mdl)
    return mdl


def parse_parameters(imdl, opts):
    if (imdl.reconst_type == 'static') or (imdl.reconst_type == 'absolute'):
        opts.abs_solve = 1
    elif imdl.reconst_type == 'difference':
        opts.abs_solve = 0
    else:
        print("error('inv_model.reconst_type ( s) not understood', imdl.reconst_type)")
    opts.select_parameters = []
    try:
        opts.select_parameters = imdl.inv_solve.select_parameters
    except(AttributeError):
        pass

    opts.reconst_to_elems = 1
    try:
        if imdl.reconst_to == 'nodes':
            opts.reconst_to_elems = 0
    except(AttributeError):
        pass
    opts.scale = 1
    try:
        opts.scale = imdl.inv_solve.scale_solution.scale
    except(AttributeError):
        pass

    opts.offset = 0
    try:
        opts.offset = imdl.inv_solve.scale_solution.offset
    except(AttributeError):
        pass
    return opts


def inv_solve(inv_model, data1, data2, opts):
    """

 INV_SOLVE: calculate imag from an inv_model and data
 inv_solve can be called as
     img= inv_solve( inv_model, data1, data2)
   if inv_model.reconst_type = 'difference'
 or
     img= inv_solve( inv_model, data )
   if inv_model.reconst_type = 'static'

   if inv_model.reconst_to = 'nodes' then output
      img.node_data has output data
   else   reconst_to = 'elems' (DEFAULT) then output to
      img.elem_data has output data

 in each case it will call the inv_model.solve

 data      is a measurement data structure
 inv_model is a inv_model structure
 img       is an image structure
           or a vector of images is data1 or data2 are vectors

 For difference EIT:
 data1      => difference data at earlier time (ie homogeneous)
 data2      => difference data at later time   (ie inhomogeneous)

 data can be:
   - an PYEIDORS data object

   - an M x S matrix, where M is the total number
         of measurements expected by inv_model

   - an M x S matrix, where M is n_elec^2
        when not using data from current injection
        electrodes, it is common to be given a full
        measurement set.  For example, 16 electrodes give
        208 measures, but 256 measure sets are common.
        Data will be selected based on fwd_model.meas_select.

 If S > 1 for both data1 and data2 then the matrix sizes must be equal

 Parameters:
   inv_model.inv_solve.select_parameters: indices of parameters to return
                         DEFAULT: return all paramteres
  Scale solution (to correct for amplitude or other defects)
   inv_model.inv_solve.scale_solution.offset
   inv_model.inv_solve.scale_solution.scale
  Disable solution error calculations
   inv_model.inv_solve.calc_solution_error = 0
    """
    inv_model = prepare_model(inv_model)
    opts = parse_parameters(inv_model, opts)
    print("inv solve: inv_solve_diff_GN_one_step")
    #  expand data sets if one is provided that is longer
    data_width = 1  # = max(num_frames(data1), num_frames(data2))
    fdata1 = data1.meas
    fdata2 = data2.meas
    imgc = inv_solve_diff_GN_one_step(inv_model, fdata1, fdata2)
    img = imgc
    img.current_params = []
    # If we reconstruct wit
    # h a different 'rec_model' then
    # put this into the img
     # Scale if required
    try:
        img.elem_data = opts.offset + opts.scale * img.elem_data
    except:
        pass
    try:
        img.node_data = opts.offset + opts.scale * img.node_data
    except:
        pass
    '''
      MATLAB IS SUPER SUPER STUPID HERE. YOU CAN'T ASSIGN IF FIELDS ARE IN
      A DIFFERENT ORDER. Example
       >> A.a= 1; A.b= 2; B.b= 3; B.a = 3; A(2) = B
    ??? Subscripted assignment between dissimilar structures.
    '''
    img.info.error = None
    # img = orderfields(img)    problem non supported on PYTHON   :)
    #  calculate residuals
    try:
        do_calc = inv_model.inv_solve.calc_solution_error
    except:
        print('inv_solve: Calculation of solution residual disabled (inv_model.inv_solve.calc_solution_error = 1 to '
              'enable)')
        do_calc = False
    if not do_calc:
        return img
    print('inv_solve: Calculating solution residual (inv_model.inv_solve.calc_solution_error = 0 to disable)')
    try:
        if opts.abs_solve:
            img.info.error = calc_solution_error(imgc, inv_model, fdata1)
        else:
            img.info.error = calc_solution_error(imgc, inv_model, fdata1, fdata2)
        print('inv_solve: Solution Error: %f', img.info.error)
    except:
        print('inv_solve: Solution Error calculation failed.')

    return img
