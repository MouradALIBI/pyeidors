import numpy as np
def get_img_data(img):
    """
 GET_IMG_DATA: get parameter data from PyEIDORS image object
 [img_data, n_images]= get_img_data(img)
 img_data - img parameter data mapped onto chosen mesh
   get_img_data looks at the elem_data or node_data
   parameters in the image. If a course2fine parameter
   exists in img.fwd_model, then it is used. 
 TODO
 FLAGS: flag = 0 (default)
    get data mapped onto elems in the fwd_model
if nargin==1; flag=0; end
    """
    try:
        img_data = [img.elem_data]
    except:
      img_data = [img.node_data]
    if (img_data[0].size == 0):
        img_data[0]=np.transpose(img_data)
    n_images = (img_data.__len__())
    try:
        c2f = img.fwd_model.coarse2fine
    except:
        return img_data, n_images


    return img_data, n_images