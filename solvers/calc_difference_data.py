import numpy as np
def calc_difference_data(data1, data2, fwd_model):
    """
  CALC_DIFFERENCE_DATA: calculate difference data between
  two eidors data vectors
  dva = calc_difference_data( meas1, meas2, fwd_model)

  dva   = Matrix n_meas x n_time_steps of difference meas
  meas1 = measurement object (or matrix) at time1 (homogeneous)
  meas2 = measurement object (or matrix) at time2 (inhomogeneous)
  fwd_model (optional, if provided in meas1 and meas2)

  if data1==0, then just process data2 like absolute data

 This code appears simple, but there are a number of tricks
  to remember, so it is best to factor it out. Issues are
  1) normalize_data, 2) remove zero meas from adjacent systems,
  3) allow both raw data and eidors_obj formats for data
    """
    # data_width = 1
    # fdata1 = data1
    # fdata2 = data2
    dva = np.asarray(data2) - np.asarray(data1)
    return dva
