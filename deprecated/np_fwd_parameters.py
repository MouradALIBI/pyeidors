import numpy as np

import itertools


# get boundary faces which match nodes

class Param:
    def __init__(self, n_elem, n_elec, n_node, n_stim, n_meas, vtx, simp, srf, df, elec, zc, indH, I, Ib):
        self.n_elem = n_elem
        self.n_elec = n_elec
        self.n_node = n_node
        self.n_stim = n_stim
        self.n_meas = n_meas
        self.vtx = vtx
        self.simp = simp
        self.srf = srf
        self.df = df
        self.elec = elec
        self.zc = zc
        self.indH = indH
        self.I = I
        self.Ib = Ib


def bdy_with_nodes(bdy, elec_nodes):
    mbdy = np.zeros(shape=(len(bdy), len(bdy[0])))
    for n in elec_nodes:
        mbdy = mbdy + (bdy == n)
    mbdy = np.transpose(mbdy)
    e_bdy = []
    for vect in mbdy:
        e_bdy.append(all(vect))
    e_bdy = np.asarray(e_bdy)
    e_bdy = e_bdy.nonzero()
    '''
     get boundary faces which match any node
     Use this for point electrodes where there are no bdy faces
     This is sort of an abuse of the model, but at least it can
     produce a reasonable result for pt electrode mdls.
    '''
    return e_bdy


def calc_param(fwd_model):
    vtx = fwd_model.nodes
    simp = fwd_model.elems
    # calc num electrodes, nodes, stim_patterns
    n_elem = len(simp)
    n_elec = len(fwd_model.electrode)
    n_node = len(fwd_model.nodes, )
    n_stim = len(fwd_model.stimulation)
    n_meas = 0

    # Recreate 'df' from fwd_model.stimulation
    # df = np.zeros(n_stim)

    df = []
    for i in range(n_stim):

        s = np.sum(np.abs(fwd_model.stimulation[i].meas_pattern)) / 2
        df.append(s)
        n_meas = n_meas + s

    zc = np.zeros(n_elec)
    srf = fwd_model.boundary
    max_elec_nodes = 0
    en_list = []
    #  get electrode parameters
    for i in range(n_elec):
        elec_nodes = fwd_model.electrode[i].nodes
        if len(elec_nodes) > 1:
            e_bdy = bdy_with_nodes(np.transpose(srf), elec_nodes)
            n_bdy = np.transpose(srf[e_bdy, :])

        else:
            n_bdy = elec_nodes
        # elec is a series of nodes matching bdy faces
        en_list.append((np.transpose(n_bdy)).flatten())
        if len(n_bdy) > max_elec_nodes:
            max_elec_nodes = len(n_bdy)
        # contact impedance
        zc[i] = fwd_model.electrode[i].z_contact
    elec = []
    for i in range(n_elec):
        elec.append(en_list[i])
    # Recreate 'indH' from fwd_model.stimulation
    indH = []
    idx = 0
    sourcepos = []
    sinkpos = []
    idx = int(n_meas)

    s0 = []

    for i in range(n_stim):
        meas_pat = np.transpose(fwd_model.stimulation[i].meas_pattern)
        sources = np.where(meas_pat == 1)
        sourcepos.append((sources[0] + len(meas_pat) * sources[1]) % n_elec + 1)

        sources = np.where(meas_pat == -1)
        sinkpos.append((sources[0] + len(meas_pat) * sources[1]) % n_elec + 1)

        # indH[idx + df[i]] = sourcepos
        # # idx = idx + int(df[i])
        s0 = sourcepos
    sss0 = np.reshape(sourcepos, (1, int(n_meas))).flatten()
    sss1 = np.reshape(sinkpos, (1, int(n_meas))).flatten()
    indH.append(sss0)
    indH.append(sss1)
    indH = np.transpose(indH)

    # calculate FEM RHS matrix, i.e., the current patterns padded with zeroes
    II = np.zeros(shape=(n_node, n_stim))

    idx = 0
    aux = []
    for i in range(n_stim):
        aux.append(fwd_model.stimulation[i].stim_pattern)
    aux = np.transpose(aux)
    I = np.concatenate((II, aux), axis=0)
    I[fwd_model.gnd_node - 1, :] = 0
    Ib = aux
    # pack into a parameter return list
    param = Param(n_elem, n_elec, n_node, n_stim, n_meas, vtx, simp, srf, df, elec, zc, indH, I, Ib)
    try:
        param.perm_sym = fwd_model.np_fwd_solve.perm_sym
    except ValueError:
        param.perm_sym = '{n}'
    param.gnd_ind = fwd_model.gnd_node
    param.normalize = 0
    return param


def np_fwd_parameters(fwd_model):
    """
     NP_FWD_PARAMETERS: data= np_fwd_solve( fwd_model )
 Extract parameters from a 'fwd_model' struct which are
 appropriate for Nick Polydorides EIDORS3D code
   param.n_elem   => number of elements
   param.n_elec   => number of electrodes
   param.n_node   => number of nodes (vertices)
   param.n_stim   => number of current stimulation patterns
   param.n_meas   => number of measurements (total)
   param.vtx      => vertex matrix
   param.simp     => connection matrix
   param.srf      => boundary triangles
   param.df       => vector of measurements for each current pattern
   param.elec     => nodes attached to each electrode
   param.zc       => vector of contact impedances
   param.indH     => electrodes used for each measurement
   param.I        => RHS (current term) for FEM solution
   param.Ib       => Current for electrodes
   param.perm_sym => 'sym' parameter
   param.gnd_ind  => node attached to ground
   param.normalize  => difference measurements normalized?

    """
    param = calc_param(fwd_model)
    return param
