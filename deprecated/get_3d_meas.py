import numpy as np


def get_3d_meas(elec, vtx, V, Ib, no_pl):
    """
     GET_3D_MEAS: extracts multiplane voltage measurements from a calculated
     3D nodal potential distribution V inside a tank with (no_pl) electrode
     planes. Each plane holds the same number of electrodes. Only the
     non-current carring electrodes at the time are involved in the
     measurements.

     [voltageH,voltageV,indH,indV,df]=get_3d_meas(elec,vtx,V,Ib,no_pl);

    elec      = The electrodes matrix
    vtx       = The vertices
    V         = The calculated forward solution
    Ib        = The current patterns without the zeroes patch
    no_pl     = The number of planes

    voltage_H = Horrisontal (local plane) measurements
    indH      = The two column matrix indicating the indices of the electrodes
                involved in voltage_H, e.g. indH = [2 3; 3 4; 4 5;...] implies
                voltage_H(1) = voltage(2) - volatge(3), etc
    df        = Array indexing the (numbero of) measurements to their corresponding
                     current patterns.
    voltage_V = Vertical interplanar measurements
    indV      = ...
   """

    if len(V[0]) != len(Ib[0]):
        print('Unmatched pattens')
    el_no = len(elec)
    q = len(elec[0])
    el_pp = int(el_no / no_pl)
    X = np.arange(el_no).reshape(no_pl, el_pp)

    Vm = V[len(vtx):len(V), :]  # Lower chunk of forward solution (complete electrode model)
    voltageH = []
    indH = []
    df = []


    for w in range(len(Vm[0])):  # For each column of Vm
        cn = 0  # RESET the count of measurements per injection
        this_inj = V[:, w]  # (no_of_electrodes x 1) vector
        for vv in range(0, el_no, el_pp):  # i.e. 1 17 33 49 for 4 planes of 16 electrodes
            # => for vv -Measurements for all electrode planes
            for t in range(vv, vv + (el_pp - 1)):  # t=1:15 => for t -Measurements of the one plane

                if Ib[t, w] == 0 and Ib[t + 1, w] == 0:  # Electrode not in the drive pair
                    voltageH.append(this_inj[t] - this_inj[t + 1])
                    indH.append([t, t + 1])
                    cn = cn + 1
                if t == vv + (el_pp - 1) - 1 and Ib[vv, w] == 0 and Ib[t + 1, w] == 0:
                    voltageH.append((this_inj[t + 1]) - this_inj[vv])  # or is it vv = 1
                    indH.append([t + 1, vv])
                    cn = cn + 1

        df.append(cn)
        voltageV = []
        indV = []
        Y = np.reshape(np.transpose(X), el_no)
        cn = 0
        wc = w
        this_inj = Vm[:, wc]  # (no_of_electrodes x 1) vector

        for ee in range(0, el_no, no_pl):
            this_chunk = Y[ee:ee + no_pl]
            for jj in range(len(this_chunk) - 1):
                if Ib[this_chunk[jj], wc] == 0 and Ib[this_chunk[jj + 1], wc] == 0:  # Electrodes not involved in currents

                    voltageV.append(this_inj[this_chunk[jj]] - int(this_inj[this_chunk[jj + 1]]))
                    indV.append([this_chunk[jj], this_chunk[jj + 1]])
                    cn = cn + 1

        df.append(cn)




    return voltageH, voltageV, indH, indV, df
