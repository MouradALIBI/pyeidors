from deprecated.fem_master_full import fem_master_full
from deprecated.np_fwd_parameters import np_fwd_parameters


def np_calc_system_mat(fwd_model, img):
    """
     NP_CALC_SYSTEM_MAT: s_mat= np_calc_system_mat( fwd_model, img)
     Fwd solver for Nick Polydorides EIDORS3D code
     s_mat.E   = FEM system matrix
     s_mat.Ela = Normalised volumes of the elements
     s_mat.D   = The sgradients of the shape functions over each element.
     s_mat.Vols= Normalised volums of the elements
     s_mat.perm= permutation of system matrix
     fwd_model = forward model
     img       = image background for system matrix calc
    """
    s_mat = []
    p = np_fwd_parameters(fwd_model)
    # Set the tolerance for the forward solver
    tol = 1e-5

    Eref, D, Ela, ppr = fem_master_full(p.vtx, p.simp, img.elem_data, p.gnd_ind, p.elec, p.zc, p.perm_sym )

    s_mat = 0
    return s_mat

